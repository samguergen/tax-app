class Tax

	def initialize(single: nil, joint: nil, separate: nil, single_agi: nil, joint_agi: nil, separate_agi: nil)
		@single = single
		@joint = joint
		@separate = separate
		@single_agi = single_agi
		@joint_agi = joint_agi
		@separate_agi = separate_agi
		@standard_deduction = 6200.00
		@join_deduction = @standard_deduction * 2
		@separate_deduction = @standard_deduction
		@standard_personal_exemption = 3950.00
		@single_pe_threshold = 254200.00
		@joint_pe_threshold = 305050.00
		@separate_pe_threshold = 152525.00

	end

	# def agi_calc

	# end

	def pe_calc
		if @single_agi
			if @single_agi <= @single_pe_threshold
				puts "smaller"
				@personal_exemption = @standard_personal_exemption
			else
				puts "larger"
				excess = (((@single_agi - @single_pe_threshold) / 2500).ceil) * 2.00
				puts excess / 100
				@personal_exemption = (excess/100) * @standard_personal_exemption
			end

		elsif @joint_agi
			if @joint_agi <= @joint_pe_threshold
				@personal_exemption = (@standard_personal_exemption * 2.00)
			else
				excess = (((@joint_agi - @joint_pe_threshold) / 2500).ceil) * 2.00
				@personal_exemption = (excess/100) * (@standard_personal_exemption * 2.00)
			end

		elsif @separate_agi
			if @separate_agi <= @separate_pe_threshold
				@personal_exemption = @standard_personal_exemption
			else
				excess = (((@separate_agi - @separate_pe_threshold) / 2500).ceil) * 2.00
				@personal_exemption = (excess/100) * @standard_personal_exemption
			end		
		end	

		puts @personal_exemption
		return @personal_exemption		

	end


	def medicare_tax(gross_income)
		@medicare_tax = (1.45/100) * gross_income
		puts @medicare_tax
	end

#gross income threshold x2 when joint?
	def fica_tax(gross_income)
		if gross_income <= 117000
			@fica = (6.20/100) * gross_income
		end

		puts @fica
	end


	def federal_tax

		single_tiers = {
			'tier1': {'income': 406750.00, 'base_tax': 118118.25, 'marginal_tax': 39.60}, 
			'tier2': {'income': 405100.00, 'base_tax': 117541.25, 'marginal_tax': 35.00},
			'tier3': {'income': 186350.00, 'base_tax': 45353.75, 'marginal_tax': 33.00},
			'tier4': {'income': 89350.00, 'base_tax': 18193.74, 'marginal_tax': 28.00},
			'tier5': {'income': 36900.00, 'base_tax': 5081.25 , 'marginal_tax': 25.00},
			'tier6': {'income': 9075.00, 'base_tax': 907.50, 'marginal_tax': 15.00}
		}

		joint_tiers = {
			'tier1': {'income': 457600.00, 'base_tax': 127962.50, 'marginal_tax': 39.60}, 
			'tier2': {'income': 405100.00, 'base_tax': 109587.50, 'marginal_tax': 35.00},
			'tier3': {'income': 226850.00, 'base_tax': 50765.00, 'marginal_tax': 33.00},
			'tier4': {'income': 148850.00, 'base_tax': 28925.00, 'marginal_tax': 28.00},
			'tier5': {'income': 73800.00, 'base_tax': 10162.50 , 'marginal_tax': 25.00},
			'tier6': {'income': 18150.00, 'base_tax': 1815.00, 'marginal_tax': 15.00}
		}

		separate_tiers = {
			'tier1': {'income': 228800.00, 'base_tax': 63981.25, 'marginal_tax': 39.60}, 
			'tier2': {'income': 202550.00, 'base_tax': 54793.75, 'marginal_tax': 35.00},
			'tier3': {'income': 113425.00, 'base_tax': 25382.50, 'marginal_tax': 33.00},
			'tier4': {'income': 74425.00, 'base_tax': 14462.50, 'marginal_tax': 28.00},
			'tier5': {'income': 36900.00, 'base_tax': 5081.25, 'marginal_tax': 25.00},
			'tier6': {'income': 9075.00, 'base_tax': 907.50, 'marginal_tax': 15.00}
		}

		if @single_agi
			if @single_agi > single_tiers[:tier1][:income]
				@fed_tax = single_tiers[:tier1][:base_tax] + ((@single_agi - single_tiers[:tier1][:income]) * (single_tiers[:tier1][:marginal_tax] / 100))
			elsif @single_agi > single_tiers[:tier2][:income]
				@fed_tax = single_tiers[:tier2][:base_tax] + ((@single_agi - single_tiers[:tier2][:income]) * (single_tiers[:tier2][:marginal_tax] / 100))
			elsif @single_agi > single_tiers[:tier3][:income]
				@fed_tax = single_tiers[:tier3][:base_tax] + ((@single_agi - single_tiers[:tier3][:income]) * (single_tiers[:tier3][:marginal_tax] / 100))
			elsif @single_agi > single_tiers[:tier4][:income]
				@fed_tax = single_tiers[:tier4][:base_tax] + ((@single_agi - single_tiers[:tier4][:income]) * (single_tiers[:tier4][:marginal_tax] / 100))
			elsif @single_agi > single_tiers[:tier5][:income]
				@fed_tax = single_tiers[:tier5][:base_tax] + ((@single_agi - single_tiers[:tier5][:income]) * (single_tiers[:tier5][:marginal_tax] / 100))
			elsif @single_agi > single_tiers[:tier6][:income]
				@fed_tax = single_tiers[:tier6][:base_tax] + ((@single_agi - single_tiers[:tier6][:income]) * (single_tiers[:tier6][:marginal_tax] / 100))
			end

		elsif @joint_agi
			if @joint_agi > joint_tiers[:tier1][:income]
				@fed_tax = joint_tiers[:tier1][:base_tax] + ((@joint_agi - joint_tiers[:tier1][:income]) * (joint_tiers[:tier1][:marginal_tax] / 100))
			elsif @joint_agi > joint_tiers[:tier2][:income]
				@fed_tax = joint_tiers[:tier2][:base_tax] + ((@joint_agi - joint_tiers[:tier2][:income]) * (joint_tiers[:tier2][:marginal_tax] / 100))
			elsif @joint_agi > joint_tiers[:tier3][:income]
				@fed_tax = joint_tiers[:tier3][:base_tax] + ((@joint_agi - joint_tiers[:tier3][:income]) * (joint_tiers[:tier3][:marginal_tax] / 100))
			elsif @joint_agi > joint_tiers[:tier4][:income]
				@fed_tax = joint_tiers[:tier4][:base_tax] + ((@joint_agi - joint_tiers[:tier4][:income]) * (joint_tiers[:tier4][:marginal_tax] / 100))
			elsif @joint_agi > joint_tiers[:tier5][:income]
				@fed_tax = joint_tiers[:tier5][:base_tax] + ((@joint_agi - joint_tiers[:tier5][:income]) * (joint_tiers[:tier5][:marginal_tax] / 100))
			elsif @joint_agi > joint_tiers[:tier6][:income]
				@fed_tax = joint_tiers[:tier6][:base_tax] + ((@joint_agi - joint_tiers[:tier6][:income]) * (joint_tiers[:tier6][:marginal_tax] / 100))
			end



		elsif @separate_agi
			if @separate_agi > separate_tiers[:tier1][:income]
				@fed_tax = separate_tiers[:tier1][:base_tax] + ((@separate_agi - separate_tiers[:tier1][:income]) * (separate_tiers[:tier1][:marginal_tax] / 100))
			elsif @separate_agi > separate_tiers[:tier2][:income]
				@fed_tax = separate_tiers[:tier2][:base_tax] + ((@separate_agi - separate_tiers[:tier2][:income]) * (separate_tiers[:tier2][:marginal_tax] / 100))
			elsif @separate_agi > separate_tiers[:tier3][:income]
				@fed_tax = separate_tiers[:tier3][:base_tax] + ((@separate_agi - separate_tiers[:tier3][:income]) * (separate_tiers[:tier3][:marginal_tax] / 100))
			elsif @separate_agi > separate_tiers[:tier4][:income]
				@fed_tax = separate_tiers[:tier4][:base_tax] + ((@separate_agi - separate_tiers[:tier4][:income]) * (separate_tiers[:tier4][:marginal_tax] / 100))
			elsif @separate_agi > separate_tiers[:tier5][:income]
				@fed_tax = separate_tiers[:tier5][:base_tax] + ((@separate_agi - separate_tiers[:tier5][:income]) * (separate_tiers[:tier5][:marginal_tax] / 100))
			elsif @separate_agi > separate_tiers[:tier6][:income]
				@fed_tax = separate_tiers[:tier6][:base_tax] + ((@separate_agi - separate_tiers[:tier6][:income]) * (separate_tiers[:tier6][:marginal_tax] / 100))
			end

		end

		
		puts @fed_tax
		return @fed_tax
	end



end


tax1 = Tax.new(single: 100000, single_agi: 89850)
tax1.federal_tax
# tax1.pe_calc
# tax1.medicare_tax(300000)
# tax1.fica_tax(100000)


#agi entered as parameter becaused based off of many variables (e.g: student loans), and NOT Standard Deduction.